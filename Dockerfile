FROM registry.sindominio.net/debian as builder

ARG TAGS "bindata"
ARG UID 110
ARG GID 65534

RUN apt-get update && \
    apt-get install -y --no-install-recommends make build-essential jq gnupg curl nodejs npm wget

## Install Go 1.21 via Go web

RUN wget https://go.dev/dl/go1.21.0.linux-amd64.tar.gz && \
		echo "d0398903a16ba2232b389fb31032ddf57cac34efda306a0eebac34f0965a0742 go1.21.0.linux-amd64.tar.gz" > go.checksum && \
		sha256sum -c go.checksum && \
		rm -rf /usr/local/go && \
		rm -rf /usr/bin/go && \
		tar -C /usr/local -xzf go1.21.0.linux-amd64.tar.gz && \
		export PATH=/usr/local/go/bin:$PATH

RUN ln -s /usr/local/go/bin/go /usr/bin/go


# Install Gitea

COPY gitea-key.asc /key.asc
RUN TGZ=`curl -s https://api.github.com/repos/go-gitea/gitea/releases/latest |jq -r '.assets[].browser_download_url' | grep 'src.*\.tar\.gz$'` && \
    filename=`basename $TGZ` && filename="${filename%.*.*}" && \
    curl -L ${TGZ} > /gitea.tar.gz && \
    curl -L ${TGZ}.asc > /gitea.tar.gz.asc && \
    gpg --import /key.asc && \
    gpg --verify /gitea.tar.gz.asc /gitea.tar.gz && \
    tar xzf /gitea.tar.gz -C /tmp/ && \
    mv /tmp/$filename /gitea

WORKDIR /gitea

# Don't clean the prebuild javascript assets
# npm fails to build them
#RUN make clean-all build

RUN TAGS="bindata sqlite sqlite_unlock_notify" make build

FROM registry.sindominio.net/debian

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
            curl gettext git ca-certificates

COPY --from=builder /gitea/gitea /app/gitea/gitea
RUN ln -s /app/gitea/gitea /usr/local/bin/gitea && \
    ln -s /data /app/gitea/data

RUN adduser \
   --system \
   --uid 110 \
   --shell /usr/sbin/nologin \
   --gecos 'Git Version Control' \
   --disabled-password \
   --home /app/gitea \
   git

VOLUME ["/data"]
ENV GITEA_CUSTOM /data
ENV HOME /data

ENTRYPOINT ["/app/gitea/gitea"]
CMD ["web", "-c", "/data/conf/app.ini"]
